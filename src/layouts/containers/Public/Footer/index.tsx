import React from 'react'

function Footer() {
  return (
    <>
      <footer className="bg-[#343647] w-full p-20">
        <img
          src="./static/images/icons/nusantech.svg"
          alt="logo"
          className="pb-6"
        />
        <div className=" flex justify-between">
          <div className="text-[#E0E0E0]">
            <p className="w-[300px] h-[84px]">
              PT Solusi Teknologi Nusantara as known as Nusantech is a company
              that focuses on information technology services and research.
            </p>
            <span className="flex gap-x-6">
              <img
                className="w-6"
                src="./static/images/icons/facebook.svg"
                alt="facebook"
              />
              <img
                className="w-6"
                src="./static/images/icons/youtube.svg"
                alt="facebook"
              />
              <img
                className="w-6"
                src="./static/images/icons/linkedin.svg"
                alt="facebook"
              />
              <img
                className="w-6"
                src="./static/images/icons/instagram.svg"
                alt="facebook"
              />
            </span>
          </div>
          <div className="flex gap-x-12 text-[#fff]">
            <span>
              <p className="mb-6 font-semibold">Services</p>
              <p>Web Development</p>
              <p>IT Professional Training Services</p>
              <p>IT Engineer Team Services</p>
            </span>
            <span>
              <p className="mb-6 font-semibold">Company</p>
              <p>Profile</p>
              <p>Contact Us</p>
              <p>Career</p>
            </span>
            <span>
              <p className="mb-6 font-semibold">Resource</p>
              <p>Blog</p>
              <p>E-book</p>
              <p>Event</p>
            </span>
            <span>
              <p className="mb-6 font-semibold">Information</p>
              <p>FAQ</p>
              <p>Testimonial</p>
            </span>
          </div>
        </div>
      </footer>
      <footer className="bg-[#1A1B24] flex justify-between">
        <span className=" py-3 px-20 gap-x-6 flex items-center justify-between text-center">
          <p className="text-[#E0E0E0] font-[400] items-center text-center p-[5px] mb-0">
            Privacy Policy
          </p>
          <p className="text-[#E0E0E0] font-[400] items-center text-center p-[5px] mb-0">
            Term of Use
          </p>
        </span>
        <span className="py-3 px-20 flex items-center justify-between text-center">
          <p className="text-[#E0E0E0] font-[400] items-center text-center p-[5px] mb-0">
            Copyright © 2023 PT. Solusi Teknologi Nusantara
          </p>
        </span>
      </footer>
    </>
  )
}

export default Footer
